import unittest
import tempfile
import program

class When_calling_loadelements_function(unittest.TestCase):

    def test_that_exception_thrown_if_unexisting_file_provided(self):
        #arrange
        filename = './unexisting_file'
        elementFactory = lambda x: x

        #act
        def act():
            program.loadelements(filename, elementFactory)

        #assert
        with self.assertRaises(FileNotFoundError):
            act()

    def test_that_empty_list_returned_if_empty_file_provided(self):
        #arrange
        with tempfile.NamedTemporaryFile(delete=False) as f:
            filename = f.name
        elementFactory = lambda x: x

        #act
        elements = program.loadelements(filename, elementFactory)

        #assert
        self.assertEqual(len(elements), 0)

    def test_that_all_elements_from_file_read(self):
        #arrange
        with tempfile.NamedTemporaryFile(delete=False, mode='w') as f:
            filename = f.name
            f.write("a\nb\nc\nd\ne\nf\ng\n")
        elementFactory = lambda x: x

        #act
        elements = program.loadelements(filename, elementFactory)

        #assert
        self.assertEqual(len(elements), 7)

    def test_that_factory_function_called(self):
        #arrange
        with tempfile.NamedTemporaryFile(delete=False, mode='w') as f:
            filename = f.name
            f.write("1\n2\n3\n4\n5\n6\n")
        elementFactory = lambda x: int(x)*int(x)

        #act
        elements = program.loadelements(filename, elementFactory)

        #assert
        self.assertEqual(elements, [x*x for x in range(1,7)])

class When_calling_comparelists_function(unittest.TestCase):

    def test_that_empty_lists_identical(self):
        #arrange
        first = []
        second = []

        #act
        result = program.comparelists(first, second)

        #assert
        self.assertTrue(result.identical())
        self.assertEqual([], result.firstonly)
        self.assertEqual([], result.secondonly)

    def test_that_all_elements_of_second_list_retured_if_first_empty(self):
        #arrange
        first = []
        second = [1, 2, 3, 3]

        #act
        result = program.comparelists(first, second)

        #assert
        self.assertFalse(result.identical())
        self.assertEqual([], result.firstonly)
        self.assertEqual([1, 2, 3, 3], result.secondonly)

    def test_that_all_elements_of_first_list_retured_if_second_empty(self):
        #arrange
        first = list('aabcd')
        second = []

        #act
        result = program.comparelists(first, second)

        #assert
        self.assertFalse(result.identical())
        self.assertEqual(list('aabcd'), result.firstonly)
        self.assertEqual([], result.secondonly)

    def test_that_same_lists_identical(self):
        #arrange
        first = ['ab', 'bc', 'cd']
        second = first

        #act
        result = program.comparelists(first, second)

        #assert
        self.assertTrue(result.identical())
        self.assertEqual([], result.firstonly)
        self.assertEqual([], result.secondonly)

    def test_that_not_found_in_second_returned_and_vice_versa(self):
        #arrange
        first = [1.1, 1.2, 1.3, 1.4, 1.5, 1.7, 1.8]
        second = [1.1, 1.2, 1.4, 1.5, 1.6, 1.8, 1.9]

        #act
        result = program.comparelists(first, second)

        #assert
        self.assertFalse(result.identical())
        self.assertEqual([1.3, 1.7], result.firstonly)
        self.assertEqual([1.6, 1.9], result.secondonly)

    def test_that_every_exceeding_entrance_of_element_returned(self):
        #arrange
        first = [(1, 'a'), (1, 'a'), (1, 'a'), (1, 'b')]
        second = [(1, 'a'), (1, 'b')]

        #act
        result = program.comparelists(first, second)

        #assert
        self.assertFalse(result.identical())
        self.assertEqual([(1, 'a'), (1, 'a')], result.firstonly)
        self.assertEqual([], result.secondonly)

    def test_that_element_order_doesnot_matter(self):
        #arrange
        first = [1, 2, 3, 0, -1]
        second = [3, 2, 1, 0, -1]

        #act
        result = program.comparelists(first, second)

        #assert
        self.assertTrue(result.identical())
        self.assertEqual([], result.firstonly)
        self.assertEqual([], result.secondonly)


class When_calling_DummyElement___init__(unittest.TestCase):

    def test_that_class_variables_set(self):
        #arrange
        url = 'http://example.com'
        hash = '1q2w3e4r5t6y7u8i9o0p'

        #act
        el = program.DummyElement(url, hash)

        #assert
        self.assertEqual(url, el.url)
        self.assertEqual(hash, el.hash)

class When_calling_DummyElement___eq__(unittest.TestCase):

    def test_that_true_returned_if_equal(self):
        #arrange
        first = program.DummyElement('http://example.com', '1q2w3e4r5t6y7u8i9o0p')
        second = program.DummyElement('http://example.com', '1q2w3e4r5t6y7u8i9o0p')

        #act
        result = first.__eq__(second)

        #assert
        self.assertTrue(result)

    def test_that_false_returned_if_url_not_equal(self):
        #arrange
        first = program.DummyElement('http://e-xample.com', '1q2w3e4r5t6y7u8i9o0p')
        second = program.DummyElement('http://example.com', '1q2w3e4r5t6y7u8i9o0p')

        #act
        result = first.__eq__(second)

        #assert
        self.assertFalse(result)

    def test_that_false_returned_if_hash_not_equal(self):
        #arrange
        first = program.DummyElement('http://example.com', '1q2w3e4r5t6y7u8i9o0p__')
        second = program.DummyElement('http://example.com', '1q2w3e4r5t6y7u8i9o0p')

        #act
        result = first.__eq__(second)

        #assert
        self.assertFalse(result)

class When_calling_DummyElement___ne__(unittest.TestCase):

    def test_that_false_returned_if_equal(self):
        #arrange
        first = program.DummyElement('http://example.com', '1q2w3e4r5t6y7u8i9o0p')
        second = program.DummyElement('http://example.com', '1q2w3e4r5t6y7u8i9o0p')

        #act
        result = first.__ne__(second)

        #assert
        self.assertFalse(result)

    def test_that_true_returned_if_url_not_equal(self):
        #arrange
        first = program.DummyElement('http://e-xample.com', '1q2w3e4r5t6y7u8i9o0p')
        second = program.DummyElement('http://example.com', '1q2w3e4r5t6y7u8i9o0p')

        #act
        result = first.__ne__(second)

        #assert
        self.assertTrue(result)

    def test_that_true_returned_if_hash_not_equal(self):
        #arrange
        first = program.DummyElement('http://example.com', '1q2w3e4r5t6y7u8i9o0p__')
        second = program.DummyElement('http://example.com', '1q2w3e4r5t6y7u8i9o0p')

        #act
        result = first.__ne__(second)

        #assert
        self.assertTrue(result)

class When_calling_DummyElement___hash__(unittest.TestCase):

    def test_that_same_hash_returned_for_same_elements(self):
        #arrange
        first = program.DummyElement('http://example.com', '1q2w3e4r5t6y7u8i9o0p')
        second = program.DummyElement('http://example.com', '1q2w3e4r5t6y7u8i9o0p')

        #act
        hash1 = first.__hash__()
        hash2 = second.__hash__()

        #assert
        self.assertEqual(hash1, hash2)

    def test_that_different_hash_returned_for_elements_with_different_url(self):
        #arrange
        first = program.DummyElement('http://e-xample.com', '1q2w3e4r5t6y7u8i9o0p')
        second = program.DummyElement('http://example.com', '1q2w3e4r5t6y7u8i9o0p')

        #act
        hash1 = first.__hash__()
        hash2 = second.__hash__()

        #assert
        self.assertNotEqual(hash1, hash2)

    def test_that_different_hash_returned_for_elements_with_different_hash(self):
        #arrange
        first = program.DummyElement('http://example.com', '1q2w3e4r5t6y7u8i9o0p__')
        second = program.DummyElement('http://example.com', '1q2w3e4r5t6y7u8i9o0p')

        #act
        hash1 = first.__hash__()
        hash2 = second.__hash__()

        #assert
        self.assertNotEqual(hash1, hash2)

class When_calling_DummyElement___repr__(unittest.TestCase):

    def test_that_returned_repr_correct(self):
        #arrange
        el = program.DummyElement('http://example.com', '1q2w3e4r5t6y7u8i9o0p')

        #act
        result = el.__repr__()

        #assert
        self.assertEqual('DummyElement(http://example.com, 1q2w3e4r5t6y7u8i9o0p)', result)

class When_calling_DummyElement_parse(unittest.TestCase):

    def test_that_returned_element_contains_parsed_values(self):
        #arrange
        rawdata = 'URL=http://example.com; HASH=1q2w3e4r5t6y7u8i9o0p'

        #act
        result = program.DummyElement.parse(rawdata)
        
        #assert
        self.assertEqual('http://example.com', result.url)
        self.assertEqual('1q2w3e4r5t6y7u8i9o0p', result.hash)


if __name__ == '__main__':
    unittest.main()