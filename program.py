import re

class DummyElement:
    parser = re.compile('^URL=([^;]*);\\s*HASH=(.*)\\s*$')

    def __init__(self, url, hash):
        self.url = url
        self.hash = hash

    def __eq__(self, other):
        return self.url == other.url and self.hash == other.hash

    def __ne__(self, other):
        return not self.__eq__(other)

    def __hash__(self):
        return (self.url.__hash__() * 397) ^ self.hash.__hash__()
    
    def __repr__(self):
        return f"DummyElement({self.url}, {self.hash})"
    
    @staticmethod
    def parse(rawdata):
        m = DummyElement.parser.match(rawdata)
        return DummyElement(m.group(1), m.group(2))

def loadelements(filename, elementFactory):
    with open(filename) as f:
        lines = f.readlines()
        elements = map(elementFactory, lines)
        return list(elements)

class ComparisonResult:
    def __init__(self, firstonly, secondonly):
        self.firstonly = firstonly
        self.secondonly = secondonly

    def identical(self):
        return not self.firstonly and not self.secondonly

def comparelists(first, second):

    dct = {x: 0 for x in first}

    for el in first: 
        dct[el] += 1

    notinfirst = []

    for el in second:
        if el not in dct or dct[el] <= 0:
            notinfirst.append(el)
        else:
            dct[el] -= 1

    multiply = lambda x,i: [x for j in range(i)]
    notinsecond = sum([multiply(el, dct[el]) for el in dct if dct[el] > 0], [])

    return ComparisonResult(notinsecond, notinfirst)

if __name__ == '__main__':
	master = loadelements('./master.data', DummyElement.parse)
	sut = loadelements('./sut.data', DummyElement.parse)
	result = comparelists(sut, master)

	if result.identical():
		print('The lists are identical')
	else:
		print('The lists are NOT identical')
		if result.firstonly:
			print('Elements found in master list, but NOT found in SUT list: ', result.firstonly)
		if result.secondonly:
			print('Elements NOT found in master list, but found in SUT list: ', result.secondonly)